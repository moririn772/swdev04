class TopController < ApplicationController
  def main
    if session[:uid].nil?
      render "main"
    else
      render "login"
    end
  end

  def login
    if User.authenticate?(tweet_params)
      session[:uid] ||= params[:uid]
      redirect_to top_main_path
    else
      redirect_to top_error_path
    end
  end

  def logout
    session.delete(:uid)
    redirect_to root_path
  end

  def error

  end

  private

  def tweet_params
    params.permit(:uid, :pass)
  end
end
