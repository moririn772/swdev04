# frozen_string_literal: true

Rails.application.routes.draw do
  root 'top#main'
  get 'top/main', to: 'top#main'
  get 'top/logout', to: 'top#logout'
  get 'top/error', to: 'top#error'
  post 'top/login', to: 'top#login'
end
